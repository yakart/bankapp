package com.ita; /**
 * 26.06.2015
 * Artur Yakubenko
 */

import com.ita.persistence.implementation.JDBC.JDBCDepartmentDAO;
import com.ita.persistence.DAO.DAOInterface.AccountDAO;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import com.ita.persistence.DAO.DAOFactory.*;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;
import com.ita.domain.Client;
import com.ita.domain.Department;
import com.ita.domain.accounts.Account;
import com.ita.domain.accounts.CheckingAccount;
import com.ita.domain.accounts.SavingAccount;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ita.persistence.io.App.*;

public class App {
    public static void main(String...args) {

        DepartmentDAO departmentDAO = null;
        AccountDAO accountDAO = null;
        ClientDAO clientDAO = null;


//        DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.Impl.COLL);
//        DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.Impl.FILE);
//        DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.Impl.XML);
//        DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.Impl.JSON);
//        DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.Impl.JDBC);
        DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.Impl.HIBER);

        while (true) {

            println("\nPlease choose number of object for operation");
            println("(to exit print '00')");
            println("1. Department");
            println("2. Client");
            println("3. Account");

            String s = readLine();
            if (s.equals("00")) {
                break;
            }

            String regex = "[1-3]";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(s);

            if (!matcher.matches()) {
                println("You should enter '1' - '2' or '00' to exit!");
                continue;
            }

            switch (parseInt(s)) {

                /*1. Department*/
                case 1: {
                    departmentDAO = factory.getDepartmentDAO();

                    while (true) {

                        println("\nPlease choose number of operation for Department");
                        println("(to exit from Department print '00')");
                        println("1. Create");
                        println("2. Read");
                        println("3. Read all");
                        println("4. Update");
                        println("5. Delete");

                        s = readLine();
                        if (s.equals("00")) {
                            break;
                        }

                        regex = "[1-5]";
                        pattern = Pattern.compile(regex);
                        matcher = pattern.matcher(s);

                        if (!matcher.matches()) {
                            println("You should enter '1' - '5' or '00' to exit!");
                            continue;
                        }

                        switch (parseInt(s)) {
                            case 1:
                                while (true) {

                                    println("\n- enter Department name: ");
                                    println("(to exit Department creation print '00')");
                                    s = readLine();
                                    if(s.equals("00")) {
                                        break;
                                    }
                                    departmentDAO.create(new Department(s));

                                }
                                break;

                            case 2:
                                println("- enter Department ID: ");
                                println(departmentDAO.readById(parseInt(readLine())));
                                break;

                            case 3:
                                for(Department department : departmentDAO.readAll()) {
                                    println(department);
                                }
                                break;

                            case 4:
                                println("- enter Department ID: ");
                                Integer ID = parseInt(readLine());

                                println("- enter Department name: ");
                                Department department = new Department(readLine());

                                departmentDAO.update(ID, department);
                                break;

                            case 5:
                                println("- enter Department ID: ");
                                departmentDAO.delete(parseInt(readLine()));
                                break;

                            default:
                                println("Please enter the number corresponding to Department operation!\n");
                                break;
                        }
                    }
                    break;
                }

                /*2. Client*/
                case 2: {
                    clientDAO = factory.getClientDAO();

                    while (true) {
                        println("\nPlease choose number of operation for Client");
                        println("(to exit from Client print '00')");
                        println("1. Create");
                        println("2. Read");
                        println("3. Read all");
                        println("4. Update");
                        println("5. Delete");

                        s = readLine();
                        if (s.equals("00")) {
                            break;
                        }

                        regex = "[1-5]";
                        pattern = Pattern.compile(regex);
                        matcher = pattern.matcher(s);

                        if (!matcher.matches()) {
                            println("You should enter '1' - '5' or '00' to exit!");
                            continue;
                        }

                        switch (parseInt(s)) {
                            /*1. Create*/
                            case 1:
                                while (true) {
                                    println("\n- enter Department ID for the client," +
                                            "\nnumber should be in a range 1 - 99:");
                                    println("(to exit Client creation print '00')");
                                    s = readLine();
                                    if (s.equals("00")) {
                                        break;
                                    }
                                    regex = "\\d{1,2}";
                                    pattern = Pattern.compile(regex);
                                    matcher = pattern.matcher(s);
                                    if (!matcher.matches()) {
                                        System.out.println("Number should be in a range 1 - 99!");
                                        continue;
                                    }

                                    Integer depId = Integer.parseInt(s);
                                    Department department = factory.getDepartmentDAO().readById(depId);

                                    println("\n- enter Client first name: ");
                                    println("(to exit Client creation print '00')");
                                    s = readLine();
                                    if (s.equals("00")) {
                                        break;
                                    }
                                    String firstName = s;

                                    println("\n- enter Client second name: ");
                                    println("(to exit Client creation print '00')");
                                    s = readLine();
                                    if (s.equals("00")) {
                                        break;
                                    }
                                    String secondName = s;

                                    println("\n- enter Client birth date in format 'dd.MM.yyyy'," +
                                            "\nyear should be in an interval 1900 - 2010:");
                                    println("(to exit Creation print '00')");
                                    s = readLine();
                                    if (s.equals("00")) {
                                        break;
                                    }
                                    regex = "(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[0-2])\\.(19[0-9]{2}|200[0-9])";
                                    pattern = Pattern.compile(regex);
                                    matcher = pattern.matcher(s);
                                    if (!matcher.matches()) {
                                        println("You should enter date in format 'dd.MM.yyyy'," +
                                                "\nyear should be in an interval 1900 - 2010!");
                                        continue;
                                    }
                                    Calendar calendar = Calendar.getInstance();
                                    Date date = null;
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                                    try {
//                                        calendar.setTime(sdf.parse(s));
                                        date = sdf.parse(s);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        continue;
                                    }
//                                    Long birthDate = calendar.getTimeInMillis();
//                                    Date birthDate = calendar.getTime();
                                    Date birthDate = date;

                                    println("\n- enter Client phone number in format 'xxx-xxx-xx-xx': ");
                                    println("(to exit Creation print '00')");
                                    s = readLine();
                                    if (s.equals("00")) {
                                        break;
                                    }
                                    String phoneNumber = s;
                                    regex = "\\d{3}-\\d{3}-\\d{2}-\\d{2}";
                                    pattern = Pattern.compile(regex);
                                    matcher = pattern.matcher(s);
                                    if (!matcher.matches()) {
                                        println("You should enter phone number in format 'xxx-xxx-xx-xx'!");
                                        continue;
                                    }

                                    clientDAO.create(new Client(firstName, secondName, birthDate, phoneNumber, department));
                                }
                                break;

                            case 2:
                                println("\n- enter Client ID: ");
                                println(clientDAO.getById(parseInt(readLine())));
                                break;

                            case 3:
                                for (Client client : clientDAO.getAll()) {
                                    println(client);
                                }

                                break;

                            case 4:
                                println("\n- enter Client ID: ");
                                println("(to exit Client updating print '00')");
                                s = readLine();
                                if (s.equals("00")) {
                                    break;
                                }
                                Integer ID = parseInt(s);

                                println("\n- enter Department ID for the client: ");
                                println("(to exit Client updating print '00')");
                                s = readLine();
                                if (s.equals("00")) {
                                    break;
                                }
                                Integer depId = Integer.parseInt(s);
                                Department department = new JDBCDepartmentDAO().readById(depId);

                                println("\n- enter Client first name: ");
                                println("(to exit Client updating print '00')");
                                s = readLine();
                                if (s.equals("00")) {
                                    break;
                                }
                                String firstName = s;

                                println("\n- enter Client second name: ");
                                println("(to exit Client updating print '00')");
                                s = readLine();
                                if (s.equals("00")) {
                                    break;
                                }
                                String secondName = s;

                                println("\n- enter Client birth date in format 'dd.MM.yyyy'," +
                                        "\nyear should be in an interval 1900 - 2010:");
                                println("(to exit Client updating print '00')");
                                s = readLine();
                                if (s.equals("00")) {
                                    break;
                                }
                                regex = "(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[0-2])\\.(19[1-9]{2}|200[0-9])";
                                pattern = Pattern.compile(regex);
                                matcher = pattern.matcher(s);
                                if (!matcher.matches()) {
                                    println("You should enter date in format 'dd.MM.yyyy'," +
                                            "\nyear should be in an interval 1900 - 2010!");
                                    continue;
                                }
                                Calendar calendar = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                                try {
                                    calendar.setTime(sdf.parse(s));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    continue;
                                }
//                                Long birthDate = calendar.getTimeInMillis();
                                Date birthDate = calendar.getTime();

                                println("\n- enter Client phone number in format 'xxx-xxx-xx-xx': ");
                                println("(to exit Client updating print '00')");
                                s = readLine();
                                if (s.equals("00")) {
                                    break;
                                }
                                String phoneNumber = s;
                                regex = "\\d{3}-\\d{3}-\\d{2}-\\d{2}";
                                pattern = Pattern.compile(regex);
                                matcher = pattern.matcher(s);
                                if (!matcher.matches()) {
                                    println("You should enter phone number in format 'xxx-xxx-xx-xx'!");
                                    continue;
                                }

                                Client client = new Client(firstName, secondName, birthDate, phoneNumber, department);

                                clientDAO.update(ID, client);
                                break;

                            case 5:
                                println("- enter Client ID: ");
                                clientDAO.delete(parseInt(readLine()));
                                break;

                            default:
                                println("Please enter the number corresponding to Client operation!\n");
                                break;
                        }
                    }
                    break;
                }

                case 3: {
                    accountDAO = factory.getAccountDAO();

                    while (true) {

                        println("\nPlease choose number of operation for Account");
                        println("(to exit from Account print '00')");
                        println("1. Create");
                        println("2. Read");
                        println("3. Read all");
                        println("4. Update");
                        println("5. Delete");

                        s = readLine();
                        if (s.equals("00")) {
                            break;
                        }

                        regex = "[1-5]";
                        pattern = Pattern.compile(regex);
                        matcher = pattern.matcher(s);

                        if(!matcher.matches()) {
                            println("You should enter '1' - '5' or '00' to exit!");
                            continue;
                        }

                        switch (parseInt(s)) {

                            case 1:
                                while (true) {

                                    println("\n- enter Account type: ");
                                    println("-- for Saving Account enter '1'");
                                    println("-- for Checking Account enter '2'");
                                    println("(to exit Account creation enter '00')");
                                    s = readLine();
                                    if (s.equals("00")) {
                                        break;
                                    }

                                    regex = "[12]";
                                    pattern = Pattern.compile(regex);
                                    matcher = pattern.matcher(s);

                                    if(!matcher.matches()) {
                                        println("You should enter '1', '2' or '00'!");
                                        continue;
                                    }

                                    Account account = null;

                                    if(s.equals("1")) {
                                        account = new SavingAccount();
                                    }
                                    if(s.equals("2")) {
                                        account = new CheckingAccount();
                                    }

                                    accountDAO.add(account);
                                }
                                break;

                            case 2:
                                println("- enter Account ID: ");
                                println(accountDAO.getById(parseInt(readLine())));
                                break;

                            case 3:
                                for (Account account : accountDAO.getAll()) {
                                    println(account);
                                }

                                break;

                            case 4:
                                println("- enter Account ID: ");
                                Integer ID = parseInt(readLine());

                                println("- enter Account type: ");
                                println("-- for Saving Account enter '1'");
                                println("-- for Checking Account enter '2'");

                                Account account = null;

                                if(s.equals("1")) {
                                    account = new SavingAccount();
                                }
                                if(s.equals("2")) {
                                    account = new CheckingAccount();
                                }

                                accountDAO.update(ID, account);
                                break;

                            case 5:
                                println("- enter Account ID: ");
                                accountDAO.delete(parseInt(readLine()));
                                break;

                            default:
                                println("Please enter the number corresponding to Account operation!\n");
                                break;
                        }
                    }
                    break;
                }

                default:
                    println("Please enter the number corresponding to one of the objects!\n");
            }
        }
    }
}
