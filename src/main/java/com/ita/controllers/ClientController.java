package com.ita.controllers;

import com.ita.domain.Client;
import com.ita.services.ClientService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
* Created by artur_000 on 06.08.2015.
*/
@Controller
public class ClientController {
    private final Logger LOGGER = Logger.getLogger(ClientController.class);

    @Autowired
    ClientService clientService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);

        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping("/index2")
    public String setupForm(Map<String, Object> map) {
        Client client = new Client();
        map.put("client", client);
        map.put("clientList", clientService.getAll());
        return "client";
    }

    @RequestMapping(value="/client.do", method= RequestMethod.POST)
    public String doActions(@ModelAttribute("client") @Valid Client client, BindingResult result,
                            @RequestParam String action, Map<String, Object> map) {
        Client clientResult = new Client();
        switch(action.toLowerCase()) {
            case "add":
                clientService.create(client);
                clientResult = client;
                if (result.hasErrors()) {
                    return "client";
                }
                break;
            case "edit":
                clientService.update(client.getId(), client);
                clientResult = client;
                if (result.hasErrors()) {
                    return "client";
                }
                break;
            case "delete":
                int id = client.getId();
                clientResult = clientService.getById(id);
                clientService.delete(id);
                break;
            case "search":
                Client searchedClient = clientService.getById(client.getId());
                clientResult = searchedClient != null ? searchedClient : new Client();
                break;
        }

        LOGGER.error("Birthday of the created client " + clientResult.getBirthDate());
//        System.out.println("Birthday of the created client " + clientResult.getBirthDate());

        map.put("client", clientResult);
        return  "successPage";
    }
}