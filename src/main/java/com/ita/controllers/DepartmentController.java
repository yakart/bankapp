package com.ita.controllers;

import com.ita.domain.Department;
import com.ita.services.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.Map;

/**
* Created by artur_000 on 06.08.2015.
*/
@Controller
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    @RequestMapping("/index")
    public String setupForm(Map<String, Object> map) {
        Department department = new Department();
        map.put("department", department);
        map.put("departmentList", departmentService.readAll());
        return "department";
    }

    @RequestMapping(value="/department.do", method= RequestMethod.POST)
    public String doActions(@ModelAttribute Department department, BindingResult result,
                            @RequestParam String action, Map<String, Object> map) {
        Department departmentResult = new Department();
        switch(action.toLowerCase()) {
            case "add":
                departmentService.create(department);
                departmentResult = department;
                break;
            case "edit":
                departmentService.update(department.getId(), department);
                departmentResult = department;
                break;
            case "delete":
                departmentService.delete(department.getId());
                departmentResult = new Department();
                break;
            case "search":
                Department searchedDepartment = departmentService.readById(department.getId());
                departmentResult = searchedDepartment != null ? searchedDepartment : new Department();
                break;
        }
        map.put("department", departmentResult);
        map.put("departmentList", departmentService.readAll());
        return "department";
    }
}