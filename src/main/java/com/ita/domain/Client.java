package com.ita.domain;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 25.06.2015
 * Artur Yakubenko
 */

@Entity
@Table/*(name="clients")*/
public class Client implements Serializable {
    private static final Long serialVersionUID = 1L;

    private static int counter;

    @Id @GeneratedValue
    private Integer id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn
    private Department department;

    @NotEmpty
    @Size(min=3, max=10)
    private String firstName;

    @NotEmpty
    @Size(min=3, max=10)
    private String lastName;

    @Temporal(value = TemporalType.DATE)
    @Past @DateTimeFormat(pattern="dd.MM.YYYY")
    private Date birthDate;

    private String phoneNumber;

    /*Its for future*/
//    private List<Account> accountList;

    public Client() {
        /*Is it good approach?*/
        department = new Department();
        department.addClient(this);
    }

    public Client(Integer id, String firstName, String lastName, Date birthDate,
                        String phoneNumber, Department department) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.department = department;
    }

    public Client(String firstName, String lastName, Date birthDate,
                  String phoneNumber, Department department) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.department = department;
    }

    public Integer getId() {
        return id;
    }

    public final void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String secondName) {
        this.lastName = secondName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setBirthDate(String birthDate) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        try {
            calendar.setTime(sdf.parse(birthDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.birthDate = calendar.getTime();
    }

//    public void setBirthDate(long birthDate) {
//        this.birthDate = birthDate;
//    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public final void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department.removeClient(this);
        this.department = department;
        this.department.addClient(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (id != client.id) return false;
        if (!birthDate.equals(client.birthDate)) return false;
        if (!department.equals(client.department)) return false;
        if (!firstName.equals(client.firstName)) return false;
        if (!lastName.equals(client.lastName)) return false;
        if (!phoneNumber.equals(client.phoneNumber)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (id != null ? id : 1);
        if(department != null) {
            result = 31 * result + department.hashCode();
        }
        if(firstName != null) {
            result = 31 * result + firstName.hashCode();
        }
        if(lastName != null) {
            result = 31 * result + lastName.hashCode();
        }
        if(birthDate != null) {
            result = 31 * result + birthDate.hashCode();
        }
        if(phoneNumber != null) {
            result = 31 * result + phoneNumber.hashCode();
        }
        return result;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String dateOfBirth = null;
        try {
//            dateOfBirth = sdf.format(new Date(birthDate));
            dateOfBirth = sdf.format(birthDate);
        } catch (NullPointerException e) {
            System.out.println(e);
        }

        return "\nClient{\nid='" + id + '\'' +
                ", department=\'" +
                department.getName() + '\'' +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + lastName + '\'' +
                ", birthDate=" + dateOfBirth +
                ", phoneNumber='" + phoneNumber + '\'' +
                "\n}";
    }

//    /*default access for usage in Account class*/
//    final void addAccount(Account account) {
//        accountList.add(account);
//    }

//    /*default access for usage in Account class*/
//    final boolean removeAccount(Account account) {
//        return accountList.remove(account);
//    }
}