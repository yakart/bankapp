package com.ita.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 29.06.2015
 * Artur Yakubenko
 */

@Entity
@Table(name="departments")
public class Department implements Serializable {
    private static final Long serialVersionUID = 1L;

    private static int counter;

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER,
//            cascade = CascadeType.PERSIST,
            mappedBy = "department")
    private List<Client> clientList;

    public Department() {
        clientList = new ArrayList<>();
    }

    public Department(String name) {
        this.name = name;
        clientList = new ArrayList<>();
    }

    public Department(Integer id, String name) {
        this.name = name;
        this.id = id;
        clientList = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }

    @Override
    public String toString() {
        return "Department{\n" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", \nclientList=" + clientList +
                "\n\n}";
    }

    /*default access for usage in Client class*/
    public final void addClient(Client client) {
        clientList.add(client);
    }

    /*default access for usage in Client class*/
    public final boolean removeClient(Client client) {
        return clientList.remove(client);
    }
}