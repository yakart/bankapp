package com.ita.domain.accounts;

import com.ita.domain.Client;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * 25.06.2015
 * Artur Yakubenko
 */


public abstract class Account implements Serializable {

    //    private static final Long serialVersionUID = 1L;

    private static int counter;

    private Client client;
//    private String id;
    private int id;

    /*default access for usage in subclasses*/
    double balance;


    public Account() {

        /*Is it good approach?*/
        client = new Client();

//        this.client.addAccount(this);
//        id = this.client.genAccountId();
        id = counter++;
    }

    public Account(Client client) {
        this.client = client;
//        this.client.addAccount(this);
//        id = this.client.genAccountId();
        id = counter++;
    }

    public Account(Client client, double balance) {
        this.client = client;
//        this.client.addAccount(this);
//        id = this.client.genAccountId();
        id = counter++;
        this.balance = balance;
    }


    /*default access for usage in subclasses of this package*/
    static final String getCurrency(double amount) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);
        return currencyFormatter.format(amount);
    }


    public final double getBalance() {
        return balance;
    }

    public final void setBalance(double balance) {
        this.balance = balance;
    }

//    public final String getId() {
//        return id;
//    }

    public final int getId() {
        return id;
    }


    public final boolean deposit(double amount) {
        if (amount > 0) {
            balance += amount;
            System.out.println("Deposition is successful!\n" +
                    "Current balance: " + getCurrency(balance));
            return true;
        } else {
            System.out.println("Please enter the positive amount in number format!");
            return false;
        }
    }


    @Override
    public String toString() {
        return "\n" + this.getClass().getSimpleName() + "{" +
                "client=" + client.getClass().getSimpleName() +
                ", id='" + id + '\'' +
                ", balance=" + getCurrency(balance) + "}";
    }

    public abstract boolean withdraw(double amount);
}