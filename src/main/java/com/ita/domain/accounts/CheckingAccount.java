package com.ita.domain.accounts;

import com.ita.domain.Client;
import com.ita.domain.Department;

/**
 * 29.06.2015
 * Artur Yakubenko
 */

public class CheckingAccount extends Account{

    private double overdraft;

    public CheckingAccount() {
        super();
    }

    public CheckingAccount(Client client) {
        super(client);
    }

    public CheckingAccount(Client client, double balance, double overdraft) {
        super(client, balance);
        this.overdraft = overdraft;
    }

    public double getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(double overdraft) {
        this.overdraft = overdraft;
    }


    @Override
    public String toString() {
        String s = super.toString();
        return  s.substring(0, s.length() - 1) +
                "; overdraft=" + getCurrency(overdraft) + "} ";
    }

    @Override
    public boolean withdraw(double amount) {
        if (((balance + overdraft) - amount) >= 0) {
            if (balance >= amount) {
                balance -= amount;
            } else {
                balance = 0;
                overdraft -= (amount - balance);
            }
            System.out.println("Withdrawal is successful!\n" +
                    "Current balance: " + getCurrency(balance));
            return true;
        } else {
            System.out.println("Not enough founds! " +
                    "Current balance: " + getCurrency(balance));
            return false;
        }
    }


    public static void main(String...args) {
        Department d1 = new Department("South");
        Department d2 = new Department("West");

        Client c1 = new Client();
        Client c2 = new Client();
        Client c3 = new Client();

        SavingAccount sa1 = new SavingAccount(c1);
        SavingAccount sa2 = new SavingAccount(c1);
        SavingAccount sa3 = new SavingAccount(c1);
        SavingAccount sa4 = new SavingAccount(c2);
        SavingAccount sa5 = new SavingAccount(c2);
        SavingAccount sa6 = new SavingAccount(c3);

        CheckingAccount ca1 = new CheckingAccount(c3);
        CheckingAccount ca2 = new CheckingAccount(c2);
        CheckingAccount ca3 = new CheckingAccount(c2);
        CheckingAccount ca4 = new CheckingAccount(c1);
        CheckingAccount ca5 = new CheckingAccount(c1);
        CheckingAccount ca6 = new CheckingAccount(c1);

        System.out.println(d1);
        System.out.println();
        System.out.println(d2);
        System.out.println();

        System.out.println(c1);
        System.out.println();
        System.out.println(c2);
        System.out.println();
        System.out.println(c3);
    }
}
