package com.ita.domain.accounts;

import com.ita.domain.Client;

/**
 * 29.06.2015
 * Artur Yakubenko
 */

public class SavingAccount extends Account {

    public SavingAccount() {
    }

    public SavingAccount(Client client) {
        super(client);
    }

    public SavingAccount(Client client, double balance) {
        super(client, balance);
    }


    @Override
    public boolean withdraw(double amount) {
        if ((balance - amount) >= 0) {
            balance -= amount;
            System.out.println("Withdrawal is successful!\n" +
                    "Current balance: " + getCurrency(balance));
            return true;
        } else {
            System.out.println("Not enough founds! " +
                    "Current balance: " + getCurrency(balance));
            return false;
        }
    }



    public static void main(String...args) {
       System.out.println(new SavingAccount(new Client()));
    }
}
