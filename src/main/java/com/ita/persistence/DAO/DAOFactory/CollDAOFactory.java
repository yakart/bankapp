package com.ita.persistence.DAO.DAOFactory;

import com.ita.persistence.implementation.collection.*;
import com.ita.persistence.DAO.DAOInterface.AccountDAO;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;

/**
 * 30.06.2015
 * Artur Yakubenko
 */

public class CollDAOFactory extends DAOFactory {
    @Override
    public AccountDAO getAccountDAO() {
        return new CollAccountDAO();
    }

    @Override
    public ClientDAO getClientDAO() {
        return new CollClientDAO();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new CollDepartmentDAO();
    }
}
