package com.ita.persistence.DAO.DAOFactory;

import com.ita.persistence.DAO.DAOInterface.AccountDAO;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;

/**
 * 30.06.2015
 * Artur Yakubenko
 */

public abstract class DAOFactory {

    public static enum Impl {
        COLL, FILE, XML, JSON, JDBC, HIBER
    }

    public static DAOFactory getDAOFactory(Impl impl) {
        switch(impl) {
            case COLL:
                return new CollDAOFactory();
            case FILE:
                return new FileDAOFactory();
            case XML:
                return new XMLDAOFactory();
            case JSON:
                return new JsonDAOFactory();
            case JDBC:
                return new JDBCDAOFactory();
            case HIBER:
                return new HiberDAOFactory();
            default:
                return null;
        }
    }

    public abstract AccountDAO getAccountDAO();

    public abstract ClientDAO getClientDAO();

    public abstract DepartmentDAO getDepartmentDAO();
}
