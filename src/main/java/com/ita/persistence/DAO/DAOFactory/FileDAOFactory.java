package com.ita.persistence.DAO.DAOFactory;


import com.ita.persistence.implementation.file.FileClientDAO;
import com.ita.persistence.implementation.file.FileDepartmentDAO;
import com.ita.persistence.DAO.DAOInterface.AccountDAO;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;
import com.ita.persistence.implementation.file.FileAccountDAO;

/**
 * 30.06.2015
 * Artur Yakubenko
 */

public class FileDAOFactory extends DAOFactory {
    @Override
    public AccountDAO getAccountDAO() {
        return new FileAccountDAO();
    }

    @Override
    public ClientDAO getClientDAO() {
        return new FileClientDAO();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new FileDepartmentDAO();
    }
}
