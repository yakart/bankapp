package com.ita.persistence.DAO.DAOFactory;

import com.ita.persistence.implementation.Hibernate.HiberClientDAO;
import com.ita.persistence.implementation.Hibernate.HiberDepartmentDAO;
import com.ita.persistence.DAO.DAOInterface.AccountDAO;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;

/**
 * Created by artur_000 on 28.07.2015.
 */
public class HiberDAOFactory extends DAOFactory {
    @Override
    public AccountDAO getAccountDAO() {
        return null;
    }

    @Override
    public ClientDAO getClientDAO() {
        return new HiberClientDAO();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new HiberDepartmentDAO();
    }
}
