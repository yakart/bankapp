package com.ita.persistence.DAO.DAOFactory;

import com.ita.persistence.implementation.JDBC.JDBCClientDAO;
import com.ita.persistence.implementation.JDBC.JDBCDepartmentDAO;
import com.ita.persistence.DAO.DAOInterface.AccountDAO;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;

/**
 * 09.07.2015
 * Artur Yakubenko
 */

public class JDBCDAOFactory extends DAOFactory {
    @Override
    public AccountDAO getAccountDAO() {
        return null;
    }

    @Override
    public ClientDAO getClientDAO() {
        return new JDBCClientDAO();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new JDBCDepartmentDAO();
    }
}
