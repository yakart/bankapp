package com.ita.persistence.DAO.DAOFactory;

import com.ita.persistence.implementation.JSON.JsonAccountDAO;
import com.ita.persistence.implementation.JSON.JsonClientDAO;
import com.ita.persistence.implementation.JSON.JsonDepartmentDAO;
import com.ita.persistence.DAO.DAOInterface.AccountDAO;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;

/**
 * 03.07.2015
 * Artur Yakubenko
 */

public class JsonDAOFactory extends DAOFactory {
    @Override
    public AccountDAO getAccountDAO() {
        return new JsonAccountDAO();
    }

    @Override
    public ClientDAO getClientDAO() {
        return new JsonClientDAO();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new JsonDepartmentDAO();
    }
}
