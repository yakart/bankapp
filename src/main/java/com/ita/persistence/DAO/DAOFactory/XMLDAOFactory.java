package com.ita.persistence.DAO.DAOFactory;

import com.ita.persistence.implementation.XML.XMLAccountDAO;
import com.ita.persistence.implementation.XML.XMLClientDAO;
import com.ita.persistence.implementation.XML.XMLDepartmentDAO;
import com.ita.persistence.DAO.DAOInterface.AccountDAO;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;

/**
 * 30.06.2015
 * Artur Yakubenko
 */

public class XMLDAOFactory extends DAOFactory {

    @Override
    public AccountDAO getAccountDAO() {
        return new XMLAccountDAO();
    }

    @Override
    public ClientDAO getClientDAO() {
        return new XMLClientDAO();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new XMLDepartmentDAO();
    }
}
