package com.ita.persistence.DAO.DAOInterface;

import com.ita.domain.accounts.Account;

import java.util.List;

/**
 * 29.06.2015
 * Artur Yakubenko
 */

public interface AccountDAO {



    /*Create a new account (for a client)*/
    void add(Account account);

    /*Get an account by ID*/
    Account getById(Integer id);

    /*Get all the accounts (of a client)*/
    List<Account> getAll();

    /*Get all the accounts sorted by ID*/
    List<Account> getAllSortedById();

    /*Update an account*/
    Account update(Integer id, Account account);

    //Delete an account by ID (from a department)*/
    Account delete(Integer id);

}
