package com.ita.persistence.DAO.DAOInterface;

import com.ita.domain.Client;

import java.util.List;

/**
 * Created by YakArt on 25.06.2015.
 */

public interface ClientDAO {
    void create(Client client);
    Client getById(Integer id);
    List<Client> getAll();
    void update(Integer id, Client client);
    void delete(Integer id);
}
