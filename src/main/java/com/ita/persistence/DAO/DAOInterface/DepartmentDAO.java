package com.ita.persistence.DAO.DAOInterface;

import com.ita.domain.Department;

import java.util.List;

/**
 * 29.06.2015
 * Artur Yakubenko
 */

public interface DepartmentDAO {
    void create(Department department);
    void update(Integer id, Department department);
    void delete(Integer id);
    Department readById(Integer id);
    List<Department> readAll();
}
