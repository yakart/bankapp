package com.ita.persistence.implementation.Hibernate;

import com.ita.domain.Client;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;

import java.util.List;

/**
* Created by artur_000 on 28.07.2015.
*/
@Repository
public class HiberClientDAO implements ClientDAO {
    @Autowired
    private SessionFactory sf;

    @Override
    public void create(Client client) {
        sf.getCurrentSession().save(client);
    }

    @Override
    public Client getById(Integer id) {
        return (Client) sf.getCurrentSession().get(Client.class, id);
    }

    @Override
    @SuppressWarnings("uncheked")
    public List<Client> getAll() {
        return (List<Client>) sf.getCurrentSession().createQuery("from Client").list();
    }

    @Override
    public void update(Integer id, Client client) {
        sf.getCurrentSession().update(client);
    }

    @Override
    public void delete(Integer id) {
        sf.getCurrentSession().delete(getById(id));
    }
}
