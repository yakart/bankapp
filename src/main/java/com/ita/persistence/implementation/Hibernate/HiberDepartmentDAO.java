package com.ita.persistence.implementation.Hibernate;

import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;
import com.ita.domain.Department;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by artur_000 on 28.07.2015.
 */

@Repository
public class HiberDepartmentDAO implements DepartmentDAO {
    @Autowired
    private SessionFactory sf;

    @Override
    public void create(Department department) {
        sf.getCurrentSession().save(department);
    }

    @Override
    public void update(Integer id, Department department) {
        sf.getCurrentSession().update(department);
    }

    @Override
    public void delete(Integer id) {
        sf.getCurrentSession().delete(readById(id));
    }

    @Override
    public Department readById(Integer id) {
        return (Department) sf.getCurrentSession().get(Department.class, id);
    }

    @Override
    public List<Department> readAll() {
        return (List<Department>) sf.getCurrentSession().createQuery("from Department").list();
    }
}

