package com.ita.persistence.implementation.JDBC;

import com.ita.domain.Client;
import com.ita.domain.Department;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 14.07.2015
 * Artur Yakubenko
 */

public class JDBCClientDAO implements ClientDAO {
    private static String table = "Clients";

    @Override
    public void create(Client client) {
        String sqlCreate =
                "INSERT INTO " + table + " "
                + "VALUES(DEFAULT, ?, ?, ?, ?, ?)";

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sqlCreate);
            ps.setString(1, client.getFirstName());
            ps.setString(2, client.getLastName());
            ps.setDate(3, new Date(client.getBirthDate().getTime()));
            ps.setString(4, client.getPhoneNumber());
            ps.setInt(5, client.getDepartment().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(ps);
        }
    }

    @Override
    public Client getById(Integer id) {
        String sqlRead =
                "SELECT "
                    + "id, first_name, second_name, "
                    + "birth_date, phone_number, dep_id "
                + "FROM "
                    + table + " WHERE id = " + id;

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        Statement statement = null;
        ResultSet rs = null;
        try {
            statement = connection.createStatement();
            rs = statement.executeQuery(sqlRead);
            rs.next();

            Integer idNum = rs.getInt("id");
            String firstName = rs.getString("first_name");
            String secondName = rs.getString("second_name");
            Date birthDate = rs.getDate("birth_date");
            String phoneNumber = rs.getString("phone_number");
            Integer depId = rs.getInt("dep_id");

            Department department = new JDBCDepartmentDAO().readById(depId);

            return new Client(idNum, firstName, secondName,
                    birthDate, phoneNumber, department);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            util.close(rs, statement);
        }
    }

    @Override
    public List<Client> getAll() {
        String sqlReadAll =
                "SELECT "
                    + "id, first_name, second_name, "
                    + "birth_date, phone_number, dep_id "
                + "FROM " + table;

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sqlReadAll);
            List<Client> clients = new ArrayList<>();

            while (resultSet.next()) {
                Integer clId = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String secondName = resultSet.getString("second_name");
                Date birthDate = resultSet.getDate("birth_date");
                String phoneNumber = resultSet.getString("phone_number");
                Integer depId = resultSet.getInt("dep_id");

                Department department = new JDBCDepartmentDAO().readById(depId);

                clients.add(new Client(clId, firstName, secondName,
                        birthDate, phoneNumber, department));
            }
            return clients;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            util.close(resultSet, statement);
        }
    }

    @Override
    public void update(Integer id, Client client) {
        String sqlUpdate =
                "UPDATE " + table + " SET "
                    + "first_name = ?, second_name = ?, birth_date = ?, "
                    + "phone_number = ?, dep_id = ? "
                + "WHERE id = " + id;

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sqlUpdate);
            ps.setString(1, client.getFirstName());
            ps.setString(2, client.getLastName());
            ps.setDate(3, new Date(client.getBirthDate().getTime()));
            ps.setString(4, client.getPhoneNumber());
            ps.setInt(5, client.getDepartment().getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(ps);
        }
    }

    @Override
    public void delete(Integer id) {
        String sqlDelete =
                "DELETE FROM " + table + " WHERE id = " + id;

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(statement);
        }
    }
}
