package com.ita.persistence.implementation.JDBC;

import com.ita.domain.Client;
import com.ita.domain.Department;
//import org.apache.log4j.Logger;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * 08.07.2015
 * Artur Yakubenko
 */

public class JDBCDepartmentDAO implements DepartmentDAO {
    private String table1 = "departments";
    private String table2 = "clients";
//    private static Logger log = Logger.getLogger(JDBCDepartmentDAO.class);

    @Override
    public void create(Department department) {
        String sqlCreate =
                "INSERT INTO " + table1 + " VALUES(DEFAULT, ?)";

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(sqlCreate);
            ps.setString(1, department.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(ps);
        }
    }

    @Override
    public Department readById(Integer id) {
//        log.error("HERE CAN BE AN ERROR!!!!!!!");
        String sqlRead =
                "SELECT "
                    + "dp.id, name, cl.id , first_name, "
                    + "second_name, birth_date, phone_number "
                + "FROM "
                    + table1 + " dp LEFT JOIN " + table2 + " cl "
                    + "ON dp.id = cl.dep_id WHERE dp.id = " + id;

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        Statement statement = null;
        ResultSet rs = null;

        try {
            statement = connection.createStatement();
            rs = statement.executeQuery(sqlRead);

            Department department = null;

            while (rs.next()) {
                if (department == null) {
                   Integer depId = rs.getInt("dp.id");
                   String depName = rs.getString("name");
                   department = new Department(depId, depName);
                }
                Integer clientId = rs.getInt("cl.id");
                if (!rs.wasNull()) {
                    String firstName = rs.getString("first_name");
                    String secondName = rs.getString("second_name");
                    String phoneNumber = rs.getString("phone_number");
                    Date birthDate = rs.getDate("birth_date");
                    Client client = new Client(clientId, firstName, secondName,
                            birthDate, phoneNumber, department);
                    department.addClient(client);
                }
            }
            return department;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            util.close(rs, statement);
        }
    }

    @Override
    public List<Department> readAll() {
        String sqlReadAll =
                "SELECT "
                    + "dp.id, name, cl.id , first_name, "
                    + "second_name, birth_date, phone_number "
                + "FROM "
                    + table1 + " dp LEFT JOIN " + table2 + " cl "
                    + "ON dp.id = cl.dep_id";

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        Statement statement = null;
        ResultSet rs = null;

        try {
            statement = connection.createStatement();
            rs = statement.executeQuery(sqlReadAll);

            Department department = null;
            Map<Integer, Department> depMap = new HashMap<>();

            while (rs.next()) {
                Integer depId = rs.getInt("dp.id");
                if (!depMap.containsKey(depId)) {
                    String depName = rs.getString("name");
                    department = new Department(depId, depName);
                    depMap.put(department.getId(), department);
                }

                Integer clientId = rs.getInt("cl.id");
                if (!rs.wasNull()) {
                    String firstName = rs.getString("first_name");
                    String secondName = rs.getString("second_name");
                    java.util.Date birthDate = rs.getDate("birth_date");
                    String phoneNumber = rs.getString("phone_number");

                    Client client = new Client(clientId, firstName, secondName,
                            birthDate, phoneNumber, department);
                    department.addClient(client);
                }
            }

            return new ArrayList<>(depMap.values());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            util.close(rs, statement);
        }
    }

    @Override
    public void update(Integer id, Department department) {
        String sqlUpdate =
                "UPDATE " + table1 + " "
                    + "SET Name = ? "
                + "WHERE ID = " + id;

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sqlUpdate);
            ps.setString(1, department.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(ps);
        }
    }

    @Override
    public void delete(Integer id) {
        String sqlDelete =
                "DELETE FROM " + table1
                + " WHERE ID = " + id;

        JDBCUtil util = new JDBCUtil();
        Connection connection = util.connect();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(statement);
        }
    }
}