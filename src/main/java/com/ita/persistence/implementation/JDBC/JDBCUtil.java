package com.ita.persistence.implementation.JDBC;

//import org.apache.log4j.Logger;

import java.sql.*;

/**
 * 10.07.2015
 * Artur Yakubenko
 */

public class JDBCUtil {
    private static int counter;
    private int id;
//    private static Logger log = Logger.getLogger(JDBCUtil.class);

    /*JDBC driver name and database URL*/
    private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private final String DB_URL = "jdbc:mysql://localhost/bankapp";

    /*Database credentials*/
    private String USER = "root";
    private String PASS = "32771122";


    public JDBCUtil() {
        id = ++counter;
    }

    public final void setCredentials(String user, String pass) {
        USER = user;
        PASS = pass;
    }

    private Connection connection = null;
//    private Statement statement = null;

    public final Connection connect() {
//        log.trace("Logging.trace: getting Connection # " + id);
        try {
            Class.forName(JDBC_DRIVER);
            System.out.println("Database connection (#" +
                    id + ")...");
            connection = DriverManager
                    .getConnection(DB_URL, USER, PASS);
            return connection;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public final void close(Statement statement) {
//        log.trace("Logging.trace: closing Connection # " + id);
        try {
            if (statement != null) {
                statement.close();
                System.out.println("Closing statement (#" +
                        id + ")...");
            }

            if (connection != null) {
                connection.close();
                System.out.println("Closing connection (#" +
                        id + ")...");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    final void close(ResultSet resultSet, Statement statement) {
//        log.error("Logging.trace: closing Connection # " + id);
        try {
            if (resultSet != null) {
                resultSet.close();
                System.out.println("Closing result set (#" +
                        id + ")...");
            }

            if (statement != null) {
                statement.close();
                System.out.println("Closing statement (#" +
                        id + ")...");
            }

            if (connection != null) {
                connection.close();
                System.out.println("Closing connection (#" +
                        id + ")...");
                System.out.println();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}