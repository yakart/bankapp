package com.ita.persistence.implementation.JSON;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.ita.domain.accounts.Account;

import com.ita.persistence.implementation.XML.XMLAccountDAO;
import com.ita.persistence.implementation.file.FileAccountDAO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * 03.07.2015
 * Artur Yakubenko
 */

public class JsonAccountDAO extends FileAccountDAO {

    private static final String fileName = "files\\JsonAccountDAO";


    protected void writeToStorage() {
        XStream xStream = new XStream(new JettisonMappedXmlDriver());
        xStream.alias("Account", Account.class);
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
           xStream.toXML(getColl(), fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void readFromStorage() {
        XStream xStream = new XStream(new JettisonMappedXmlDriver());
        xStream.alias("XMLAccountDAO", XMLAccountDAO.class);
        try (FileInputStream fis = new FileInputStream(fileName)) {
            setColl((HashMap<Integer, Account>) xStream.fromXML(fis));
        } catch (FileNotFoundException e1) {
            System.out.println("File was not found," +
                    "so the new one has been created.");
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
