package com.ita.persistence.implementation.JSON;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.ita.domain.Department;
import com.ita.persistence.implementation.file.FileDepartmentDAO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * 03.07.2015
 * Artur Yakubenko
 */

public class JsonDepartmentDAO extends FileDepartmentDAO {
    private static final String fileName = "files\\XMLDepartmentDAO.xml";

    @Override
    protected void writeToStorage() {
        XStream xStream = new XStream(new JettisonMappedXmlDriver());
        xStream.alias("Department", Department.class);
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            xStream.toXML(getColl(), fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void readFromStorage() {
        XStream xStream = new XStream(new JettisonMappedXmlDriver());
        xStream.alias("Department", Department.class);
        try (FileInputStream fis = new FileInputStream(fileName)) {
            setColl((HashMap<Integer, Department>) xStream.fromXML(fis));
        } catch (FileNotFoundException e1) {
            System.out.println("File was not found," +
                    "so the new one has been created.");
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
