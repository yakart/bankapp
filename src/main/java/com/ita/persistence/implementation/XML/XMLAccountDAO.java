package com.ita.persistence.implementation.XML;

import com.thoughtworks.xstream.XStream;
import com.ita.domain.accounts.Account;
import com.ita.persistence.implementation.file.FileAccountDAO;

import java.io.*;
import java.util.HashMap;

/**
 * 03.07.2015
 * Artur Yakubenko
 */

public class XMLAccountDAO extends FileAccountDAO {

    private static final String fileName = "files\\XMLAccountDAO";

    @Override
    protected void writeToStorage() {
        XStream xStream = new XStream();
        xStream.alias("Account", Account.class);
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
           xStream.toXML(getColl(), fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void readFromStorage() {
        XStream xStream = new XStream();
        xStream.alias("Account", Account.class);
        try (FileInputStream fis = new FileInputStream(fileName)) {
            setColl((HashMap<Integer, Account>) xStream.fromXML(fis));
        } catch (FileNotFoundException e1) {
            System.out.println("File was not found," +
                    "so the new one has been created.");
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
