package com.ita.persistence.implementation.XML;

import com.thoughtworks.xstream.XStream;
import com.ita.domain.Department;
import com.ita.persistence.implementation.file.FileDepartmentDAO;

import java.io.*;
import java.util.HashMap;

/**
 * 03.07.2015
 * Artur Yakubenko
 */

public class XMLDepartmentDAO extends FileDepartmentDAO {
    private static final String fileName = "files\\XMLDepartmentDAO.xml";

    @Override
    protected void writeToStorage() {
        XStream xStream = new XStream();
        xStream.alias("Department", Department.class);
        try (FileOutputStream fos = new FileOutputStream(fileName);
                ObjectOutputStream ous = new ObjectOutputStream(fos)) {
            xStream.toXML(getColl(), fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void readFromStorage() {
        XStream xStream = new XStream();
        xStream.alias("Department", Department.class);
        try (FileInputStream fis = new FileInputStream(fileName);
                ObjectInputStream ois = new ObjectInputStream(fis)) {
            setColl((HashMap<Integer, Department>) xStream.fromXML(fis));
        } catch (FileNotFoundException e1) {
            System.out.println("File was not found," +
                    "so the new one has been created.");
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}