package com.ita.persistence.implementation.collection;

import com.ita.domain.accounts.Account;
import com.ita.persistence.DAO.DAOInterface.AccountDAO;

import java.util.*;

/**
 * 29.06.2015
 * Artur Yakubenko
 */

public class CollAccountDAO implements AccountDAO {

    private static Map<Integer, Account> coll = new HashMap<>();


    protected final Map<Integer, Account> getColl() {
        return coll;
    }

    protected final void setColl(Map<Integer, Account> newStorage) {
        coll = newStorage;
    }

    @Override
    public void add(Account account) {
        coll.put(account.getId(), account);
    }

    @Override
    public Account getById(Integer id) {
        return coll.get(id);
    }

    @Override
    public List<Account> getAll() {
        ArrayList<Account> accounts = new ArrayList<>(coll.values());
        return Collections.unmodifiableList(accounts);
    }

    @Override
    public List<Account> getAllSortedById() {
        ArrayList<Account> accounts = new ArrayList<>(coll.values());
        Collections.sort(accounts, new CompByID());
        return Collections.unmodifiableList(accounts);
    }

    @Override
    public Account update(Integer ID, Account account) {
        return coll.replace(ID, account);
    }

    @Override
    public Account delete(Integer ID) {
        return coll.remove(ID);
    }


    private static class CompByID implements Comparator<Account> {

        @Override
        public int compare(Account o1, Account o2) {
            return o1.getId() - (o2.getId());
        }
    }
}