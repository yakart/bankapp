package com.ita.persistence.implementation.collection;

import com.ita.domain.Client;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;

import java.util.*;

/**
 * 26.06.2015
 * Artur Yakubenko
 */

public class CollClientDAO implements ClientDAO {

    private static Map<Integer, Client> coll = new HashMap<>();


    protected final Map<Integer, Client> getColl() {
        return coll;
    }

    protected final void setColl(Map<Integer, Client> newStorage) {
        coll = newStorage;
    }

    @Override
    public void create(Client client) {
        coll.put(client.getId(), client);
    }

    @Override
    public Client getById(Integer ID) {
        return coll.get(ID);
    }

//    @Override
//    public Client getByName(String name) {
//        Client client = null;
//
//        for(Client c : coll.values()) {
//            if(c.getFirstName().equals(name)) {
//                client = c;
//            }
//        }
//        return client;
//    }

    @Override
    public List<Client> getAll() {
        ArrayList<Client> clients = new ArrayList<>(coll.values());
        return Collections.unmodifiableList(clients);
    }

//    @Override
//    public List<Client> getAllSortedByName() {
//        ArrayList<Client> clients = new ArrayList<>(coll.values());
//        Collections.sort(clients, new CompByName());
//        return Collections.unmodifiableList(clients);
//    }

//    @Override
//    public List<Client> getAllSortedByAge() {
//        ArrayList<Client> clients = new ArrayList<>(coll.values());
//        Collections.sort(clients, new CompByAge());
//        return Collections.unmodifiableList(clients);
//    }

    @Override
    public void update(Integer ID, Client client) {
        coll.put(ID, client);
    }

    @Override
    public void delete(Integer ID) {
        coll.remove(ID);
    }

    private static class CompByName implements Comparator<Client> {

        @Override
        public int compare(Client o1, Client o2) {
            return o1.getFirstName().compareTo(o2.getFirstName());
        }
    }

    private static class CompByAge implements Comparator<Client> {

        @Override
        public int compare(Client o1, Client o2) {
            return o1.getBirthDate().compareTo(o2.getBirthDate());
        }
    }
}