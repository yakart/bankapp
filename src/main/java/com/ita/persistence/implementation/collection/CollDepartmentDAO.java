package com.ita.persistence.implementation.collection;

import com.ita.domain.Department;
import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;

import java.util.*;

/**
 * 29.06.2015
 * Artur Yakubenko
 */

public class CollDepartmentDAO implements DepartmentDAO {

    private static Map<Integer, Department> coll = new HashMap<>();


    protected final Map<Integer, Department> getColl() {
        return coll;
    }

    protected final void setColl(Map<Integer, Department> newStorage) {
        coll = newStorage;
    }

    @Override
    public void create(Department department) {
        coll.put(department.getId(), department);
    }

    @Override
    public Department readById(Integer id) {
        return coll.get(id);
    }

    @Override
    public List<Department> readAll() {
        List<Department> departments = new ArrayList<>(coll.values());
        return Collections.unmodifiableList(departments);
    }

//    @Override
//    public List<department> getAllSortedByID() {
//        List<department> departments = new ArrayList<>(coll.values());
//        Collections.sort(departments, new CompByID());
//        return Collections.unmodifiableList(departments);
//    }

    @Override
    public void update(Integer id, Department department) {
        coll.replace(id, department);
    }

    @Override
    public void delete(Integer id) {
        coll.remove(id);
    }


    private static class CompByID implements Comparator<Department> {
        @Override
        public int compare(Department o1, Department o2) {
            return o1.getId() - o2.getId();
        }
    }
}