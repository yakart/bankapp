package com.ita.persistence.implementation.file;

import com.ita.domain.accounts.Account;
import com.ita.persistence.implementation.collection.CollAccountDAO;

import java.io.*;
import java.util.HashMap;
import java.util.List;

/**
 * 02.07.2015
 * Artur Yakubenko
 */

public class FileAccountDAO extends CollAccountDAO {

    private static final String fileName = "files\\FileAccountDAO";


    protected void writeToStorage() {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream ous = new ObjectOutputStream(fos))  {
            ous.writeObject(getColl());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void readFromStorage() {
        try (FileInputStream fis = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            setColl((HashMap<Integer, Account>) ois.readObject());
        } catch (FileNotFoundException e1) {
            System.out.println("File was not found," +
                    "so the new one has been created.");
        } catch (ClassNotFoundException | IOException e2) {
            e2.printStackTrace();
        }
    }

    @Override
    public void add(Account account) {
        readFromStorage();
        super.add(account);
        writeToStorage();
    }

    @Override
    public Account getById(Integer ID) {
        readFromStorage();
        return super.getById(ID);
    }

    @Override
    public List<Account> getAll() {
        readFromStorage();
        return super.getAll();
    }

    @Override
    public List<Account> getAllSortedById() {
        readFromStorage();
        return super.getAllSortedById();
    }

    @Override
    public Account update(Integer ID, Account account) {
        readFromStorage();
        Account temp = super.update(ID, account);
        writeToStorage();
        return temp;
    }

    @Override
    public Account delete(Integer ID) {
        readFromStorage();
        Account temp = super.delete(ID);
        writeToStorage();
        return temp;
    }
}