package com.ita.persistence.implementation.file;

import com.ita.domain.Client;
import com.ita.persistence.implementation.collection.CollClientDAO;

import java.io.*;
import java.util.HashMap;
import java.util.List;

/**
 * 02.07.2015
 * Artur Yakubenko
 */

public class FileClientDAO extends CollClientDAO {

    private static final String fileName = "files\\FileClientDAO";


    protected void writeToStorage() {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream ous = new ObjectOutputStream(fos))  {
            ous.writeObject(getColl());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void readFromStorage() {
        try (FileInputStream fis = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            setColl((HashMap<Integer, Client>) ois.readObject());
        } catch (FileNotFoundException e1) {
            System.out.println("File was not found," +
                    "so the new one has been created.");
        } catch (IOException | ClassNotFoundException e2) {
            e2.printStackTrace();
        }
    }


    @Override
    public void create(Client client) {
        readFromStorage();
        super.create(client);
        writeToStorage();
    }

    @Override
    public Client getById(Integer ID) {
        readFromStorage();
        Client temp = super.getById(ID);
        writeToStorage();
        return temp;
    }

//    @Override
//    public Client getByName(String name) {
//        readFromStorage();
//        Client temp = super.getByName(name);
//        writeToStorage();
//        return temp;
//    }

    @Override
    public List<Client> getAll() {
        readFromStorage();
        List<Client> temp = super.getAll();
        writeToStorage();
        return temp;
    }

//    @Override
//    public List<Client> getAllSortedByName() {
//        readFromStorage();
//        List<Client> temp = super.getAllSortedByName();
//        writeToStorage();
//        return temp;
//    }

//    @Override
//    public List<Client> getAllSortedByAge() {
//        readFromStorage();
//        List<Client> temp = super.getAllSortedByAge();
//        writeToStorage();
//        return temp;
//    }

    @Override
    public void update(Integer ID, Client client) {
        readFromStorage();
        super.update(ID, client);
        writeToStorage();
    }

    @Override
    public void delete(Integer ID) {
        readFromStorage();
        super.delete(ID);
        writeToStorage();
    }
}
