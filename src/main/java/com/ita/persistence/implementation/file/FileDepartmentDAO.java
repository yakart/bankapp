package com.ita.persistence.implementation.file;

import com.ita.domain.Department;
import com.ita.persistence.implementation.collection.CollDepartmentDAO;

import java.io.*;
import java.util.HashMap;
import java.util.List;

/**
 * 02.07.2015
 * Artur Yakubenko
 */

public class FileDepartmentDAO extends CollDepartmentDAO {

    private static final String fileName = "files\\FileDepartmentDAO";


    protected void writeToStorage() {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream ous = new ObjectOutputStream(fos))  {
            ous.writeObject(getColl());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void readFromStorage() {
        try (FileInputStream fis = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            setColl((HashMap<Integer, Department>) ois.readObject());
        } catch (FileNotFoundException e1) {
            System.out.println("File was not found," +
                    "so the new one has been created.");
        } catch (IOException | ClassNotFoundException e2) {
            e2.printStackTrace();
        }
    }


    @Override
      public void create(Department department) {
        readFromStorage();
        super.create(department);
        writeToStorage();
    }

    @Override
    public Department readById(Integer id) {
        readFromStorage();
        Department temp = super.readById(id);
        writeToStorage();
        return temp;
    }

    @Override
    public List<Department> readAll() {
        readFromStorage();
        List<Department> temp = super.readAll();
        writeToStorage();
        return temp;
    }

//    @Override
//    public List<department> getAllSortedByID() {
//        readFromStorage();
//        List<department> temp = super.getAllSortedByID();
//        writeToStorage();
//        return temp;
//    }

    @Override
    public void update(Integer id, Department department) {
        readFromStorage();
        super.update(id, department);
        writeToStorage();
    }

    @Override
    public void delete(Integer id) {
        readFromStorage();
        super.delete(id);
        writeToStorage();
    }
}

