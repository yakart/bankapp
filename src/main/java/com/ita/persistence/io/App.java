package com.ita.persistence.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 29.06.2015
 * Artur Yakubenko
 */

public class App {

    private static BufferedReader br =
            new BufferedReader(new InputStreamReader(System.in));

    public static <T> void println(T t) {
        System.out.println(t);
    }

    public static <T> void print(T t) {
        System.out.print(t);
    }


    public static String readLine() {
        try {
            return br.readLine();
        } catch (IOException e) {
            println(e);
            return null;
        }
    }

    public static Integer parseInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            println(e);
            return 100;
        }
    }
}

