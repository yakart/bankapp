package com.ita.services;

import com.ita.domain.Client;

import java.util.List;

/**
* Created by artur_000 on 06.08.2015.
*/
public interface ClientService {
    public void create(Client client);
    public Client getById(Integer id);
    public List<Client> getAll();
    public void update(Integer id, Client client);
    public void delete(Integer id);
}
