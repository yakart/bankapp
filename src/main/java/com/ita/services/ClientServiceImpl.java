package com.ita.services;

import com.ita.domain.Client;
import com.ita.persistence.DAO.DAOInterface.ClientDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* Created by artur_000 on 06.08.2015.
*/
@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientDAO clientDAO;

    @Transactional
    public void create(Client client) {
        clientDAO.create(client);
    }

    @Transactional
    public Client getById(Integer id) {
        return clientDAO.getById(id);
    }

    @Transactional
    public List<Client> getAll() {
        return clientDAO.getAll();
    }

    @Transactional
    public void update(Integer id, Client client) {
        clientDAO.update(id, client);
    }

    @Transactional
    public void delete(Integer id) {
        clientDAO.delete(id);
    }
}
