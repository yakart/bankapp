package com.ita.services;

import com.ita.domain.Department;

import java.util.List;

/**
 * Created by artur_000 on 05.08.2015.
 */
public interface DepartmentService {
    public void create(Department department);
    public void update(Integer id, Department department);
    public void delete(Integer id);
    public Department readById(Integer id);
    public List<Department> readAll();
}
