package com.ita.services;

import com.ita.persistence.DAO.DAOInterface.DepartmentDAO;
import com.ita.domain.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* Created by artur_000 on 05.08.2015.
*/

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentDAO departmentDAO;

    @Transactional
    public void create(Department department) {
        departmentDAO.create(department);
    }

    @Transactional
    public Department readById(Integer id) {
        return departmentDAO.readById(id);
    }

    @Transactional
    public List<Department> readAll() {
        return departmentDAO.readAll();
    }

    @Transactional
    public void update(Integer id, Department department) {
        departmentDAO.update(id, department);
    }

    @Transactional
    public void delete(Integer id) {
        departmentDAO.delete(id);
    }
}
