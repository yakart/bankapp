<%--
  Created by IntelliJ IDEA.
  User: artur_000
  Date: 06.08.2015
  Time: 11:50
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@ include file="includes.jsp" %>

<html>
<head>
  <title>Client Management</title>
</head>
<body>
<h1>Clients Data</h1>
<form:form action="client.do" commandName="client" method="post">
  <table>
    <tr>
      <td>Client ID</td>
      <td><form:input placeholder="Don't enter for \"add\"" path="id" /></td>
    </tr>
    <tr>
      <td>First name</td>
      <td><form:input placeholder="James" path="firstName" /></td>
      <td><font color="red"><form:errors path="firstName" /></font></td>
    </tr>
    <tr>
      <td>Last name</td>
      <td><form:input placeholder="Bond" path="lastName" /></td>
      <td><font color="red"><form:errors path="lastName" /></font></td>
    </tr>
    <tr>
      <td>Birth date</td>
      <td><form:input type="date" path="birthDate" /></td>
    </tr>
    <tr>
      <td>Phone number</td>
      <td><form:input type="tel" placeholder="063-555-1212" pattern="\d{3}-\d{3}-\d{4}" path="phoneNumber" /></td>
    </tr>
    <tr>
      <td>Department ID</td>
      <td><form:input type="number" path="department.id" /></td>
    </tr>
    <tr>
      <td colspan="2">
        <input type="submit" name="action" value="Add" />
        <input type="submit" name="action" value="Edit" />
        <input type="submit" name="action" value="Delete" />
        <input type="submit" name="action" value="Search" />
      </td>
    </tr>
  </table>
  <br>
  <table border="1">
    <tr>
      <th>Client ID</th>
      <th>First name</th>
      <th>Last name</th>
      <th>Birth date</th>
      <th>Phone number</th>
      <th>Department</th>
    </tr>
    <c:forEach items="${clientList}" var="client">
      <tr>
        <td>${client.id}</td>
        <td>${client.firstName}</td>
        <td>${client.lastName}</td>
        <td>${client.birthDate}</td>
        <td>${client.phoneNumber}</td>
        <td>${client.department.name}</td>
      </tr>
    </c:forEach>
  </table>
</form:form><br><br>

<p><spring:message code="label.registration"/></p>
<p><spring:message code="label.login"/></p> <br><br>

<a href="/?lang=en">en</a> | <a href="/?lang=ru">ru</a>

<p><b><a href="/home">Home page</a></b></p>
</body>
</html>
