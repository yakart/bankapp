<%--
  Created by IntelliJ IDEA.
  User: artur_000
  Date: 06.08.2015
  Time: 11:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
          pageEncoding="utf8"%>
<%@ include file="includes.jsp" %>

<html>
<head>
    <title>Department Management</title>
</head>
<body>
<h1>Departments Data</h1>
<form:form action="department.do" method="post" commandName="department">
  <table>
    <tr>
      <td>Department ID</td>
      <td><form:input path="id" /></td>
    </tr>
    <tr>
      <td>Department Name</td>
      <td><form:input path="name" /></td>
    </tr>
    <tr>
      <td colspan="2">
        <input type="submit" name="action" value="Add" />
        <input type="submit" name="action" value="Edit" />
        <input type="submit" name="action" value="Delete" />
        <input type="submit" name="action" value="Search" />
      </td>
    </tr>
  </table>
  <br>
  <table border="1">
    <tr>
      <th>ID</th>
      <th>Name</th>
    </tr>
    <c:forEach items="${departmentList}" var="department">
      <tr>
        <td>${department.id}</td>
        <td>${department.name}</td>
      </tr>
    </c:forEach>
  </table>
</form:form>

<p><b><a href="/home">Home page</a></b></p>
</body>
</html>