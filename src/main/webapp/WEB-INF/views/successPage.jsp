<%--
  Created by IntelliJ IDEA.
  User: artur_000
  Date: 12.08.2015
  Time: 6:18
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Form Processed Successfully!</title>
</head>
<body>
Form has been processed for the following client: <br><br>
<table border="1">
  <tr>
    <th>Client ID</th>
    <th>First name</th>
    <th>Last name</th>
    <th>Birth date</th>
    <th>Phone number</th>
    <th>Department</th>
  </tr>
  <tr>
    <td>${client.id}</td>
    <td>${client.firstName}</td>
    <td>${client.lastName}</td>
    <td>${client.birthDate}</td>
    <td>${client.phoneNumber}</td>
    <td>${client.department.name}</td>
  </tr>
</table> <br><br>
<a href="<c:url value='/index2'/>">Back to form</a><br>
<a href="<c:url value='/home'/>">Home page</a>
</body>
</html>
